import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContactPage } from '../pages/contact/contact';
import { LoginPage } from '../pages/login/login';
import { MessagePage } from '../pages/message/message';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// export const firebaseConfig = {
//   apiKey: "AIzaSyCJiPAtIlgR2GNyb8kgAR_MERFPIEgDSF0",
//   authDomain: "message-e736a.firebaseapp.com",
//   databaseURL: "https://message-e736a.firebaseio.com",
//   projectId: "message-e736a",
//   storageBucket: "message-e736a.appspot.com",
//   messagingSenderId: "311152053380",
//   appId: "1:311152053380:web:71299d3a9ec64810"
// };
export const firebaseConfig = {
  apiKey: "AIzaSyC-HR6p2F_-GFqAIYSkM_e1Hx0eG3BSSIk",
  authDomain: "test-ionic-6a249.firebaseapp.com",
  databaseURL: "https://test-ionic-6a249.firebaseio.com",
  projectId: "test-ionic-6a249",
  storageBucket: "test-ionic-6a249.appspot.com",
  messagingSenderId: "423953856454",
  appId: "1:423953856454:web:e7155b15128f2579"
};

@NgModule({
  declarations: [
    LoginPage,
    MyApp,
    HomePage,
    ContactPage,
    MessagePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig,'test_ionic'),
    AngularFireAuthModule,
    AngularFireDatabaseModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPage,
    MyApp,
    HomePage,
    ContactPage, MessagePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
