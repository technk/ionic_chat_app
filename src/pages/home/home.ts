import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactPage } from '../contact/contact';
import { Observable } from 'rxjs';
import { LoginPage } from '../login/login';
import { MessagePage } from '../message/message';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items: Observable<any[]>;
  items_list: any = [];
  currentUser: any;
  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public fireauth: AngularFireAuth, private formBuilder: FormBuilder, public firedb: AngularFireDatabase) {
    this.fireauth.authState.subscribe(user => {
      if (user.email != null) {
        this.currentUser = user;
      }else{
        this.navCtrl.setRoot(LoginPage);
      }
    });  


    // this.firedb.object('contact').snapshotChanges().subscribe(action => {
    //   console.log(action.type);
    //   console.log(action.key)
    //   console.log(action.payload.val())
    // })
  }
  ionViewDidLoad() {  
    this.firedb.object('contact/' + this.currentUser.uid).valueChanges().subscribe(data => {
      if (data != null && data != undefined) {
        var keys = Object.keys(data);
        for (let index = 0; index < keys.length; index++) {
          const element = data[keys[index]];
          this.items_list.push(element);
        }
      }
    });

    // this.firedb.object('contact/' + this.currentUser.uid).snapshotChanges().subscribe(data=>{
    //   console.log(data.payload);      
    // })
  }


  contactpagesebt() {
    this.navCtrl.push(ContactPage)
  }
  messagepagepassing(data) {
    console.log(data);    
    this.navCtrl.push(MessagePage, { 'item': data});
    // const modal = this.modalCtrl.create(MessagePage, { 'item': data });
    // modal.present();
  }
  signlogout(){
    this.fireauth.auth.signOut();
    this.navCtrl.setRoot(LoginPage);
  }
}
