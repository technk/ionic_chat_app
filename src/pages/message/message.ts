import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {
  value :any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, public alertCtrl: AlertController) {
    this.value = navParams.get('item');
    console.log(this.value);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');
  }

  opencamera(){
    try {
      this.alert('App Running');
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        console.log(base64Image);

      }, (err) => {
        // Handle error
          this.alert(err);
      });
    } catch (error) {
      this.alert(error);
    }
  }

  alert(message: any) {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: message,
      buttons: ['Ok'],
    }).present();
  }
}
