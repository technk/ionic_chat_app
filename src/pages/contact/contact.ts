import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  private todo: FormGroup;
  currentUser : any;
  items: Observable<any[]>;
  constructor(public navCtrl: NavController,public toastCtrl: ToastController, public fireauth: AngularFireAuth, public firedatabase: AngularFireDatabase, public navParams: NavParams, private formBuilder: FormBuilder) {
        // console.log(data,keys.length);
    this.fireauth.authState.subscribe(user => {
      if (user.email != null) {
        this.currentUser = user;
        // this.navCtrl.setRoot(HomePage);
      } else {
        this.navCtrl.setRoot(LoginPage);
      }
      console.log(this.currentUser);
    })   
   
    this.todo = this.formBuilder.group({
      username: ['', Validators.required],
      mobile: ['', Validators.required],
    });
  }

  contact: any;
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
  logForm() {
    this.firedatabase.list('contact/'+this.currentUser.uid).push(this.todo.value);
    this.todo.reset();
    this.navCtrl.setRoot(HomePage);
    console.log(this.todo.value);
    this.showToast('Contact Added Successfully');
  }
  showToast(message: any) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}
