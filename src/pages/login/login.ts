import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private todo: FormGroup;
  private register: FormGroup;
  loginFormTrue: any;
  currentUser: any;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public toastCtrl: ToastController, private formBuilder: FormBuilder, public navParams: NavParams, public fireauth: AngularFireAuth) {

    // fireauth.auth.signOut();

    // fireauth.authState.subscribe(user => {
    //   if(user.email != null){
    //     this.currentUser = user;
    //     this.navCtrl.setRoot(HomePage);
    //   }
    //   console.log(this.currentUser);
    // });
    this.onAutoCallPage();

    this.loginFormTrue = 'signin';
    this.todo = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.register = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      re_password: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  segmentChanged(ev: any) {
    this.loginFormTrue = ev.value;
  }
  Login() {

    console.log(this.todo.value);
    this.fireauth.auth.signInWithEmailAndPassword(this.todo.value.email, this.todo.value.password)
      .then(data => {
        console.log(data, this.fireauth.auth);
        this.navCtrl.setRoot(HomePage);
this.alert("Success! You\`re logged in");
      }).catch(error => {
        console.log(error);
        this.showToast(error.message);
      });

  }
  RegisterStore() {
    console.log(this.register.value);

    if (this.register.value.password == this.register.value.re_password) {
      this.fireauth.auth.createUserWithEmailAndPassword(this.register.value.email, this.register.value.password)
        .then(data => {
          console.log(data);
          this.navCtrl.setRoot(HomePage);
          this.alert("Success! You\`re logged in");
          console.log(this.fireauth.auth.currentUser);
        }).catch(error => {
          console.log(error);
          this.showToast(error.message);
        });
    } else {
      this.showToast('Password is incorrect');
    }

  }

  showToast(message: any) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  alert(message: any) {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: message,
      buttons: ['Ok'],
    }).present();
  }

  onAutoCallPage(){
    this.fireauth.authState.subscribe( user =>{
      if (user.email != null) {
        this.currentUser = user;
        this.navCtrl.setRoot(HomePage);
      }
      console.log(this.currentUser);
    })
  }

}
